package com.yihangho.jelly.configurations

import com.yihangho.jelly.properties.HandlersProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class HandlersConfiguration(private val properties: HandlersProperties) {

    @Bean
    fun commandConfiguration() = properties.command

    @Bean
    fun githubConfiguration() = properties.github

    @Bean
    fun jiraConfiguration() = properties.jira

    @Bean
    fun rewriteConfiguration() = properties.rewrite

}
