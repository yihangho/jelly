package com.yihangho.jelly

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class JellyApplication

fun main(args: Array<String>) {
    runApplication<JellyApplication>(*args)
}
