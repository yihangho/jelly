package com.yihangho.jelly.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "jelly.synonyms")
data class SynonymsProperties(
    val entries: List<SynonymEntry>
)

data class SynonymEntry(
    val keywords: Set<String>,
    val properties: Map<String, String>
)
