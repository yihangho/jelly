package com.yihangho.jelly.properties

import com.yihangho.jelly.handlers.CommandHandler
import com.yihangho.jelly.handlers.GitHubHandler
import com.yihangho.jelly.handlers.JiraHandler
import com.yihangho.jelly.handlers.RewriteHandler
import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "jelly.handlers")
data class HandlersProperties(
    val command: CommandHandler.Configuration,
    val github: GitHubHandler.Configuration,
    val jira: JiraHandler.Configuration,
    val rewrite: RewriteHandler.Configuration
)
