package com.yihangho.jelly.handlers

import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

val TASK_ID_PATTERN = Regex("""^([A-Z]+)-\d+$""")

@Component
@Order(value = 0)
class JiraHandler(private val configuration: Configuration) : IHandler {

    override fun handle(input: String): String? {
        val match = TASK_ID_PATTERN.matchEntire(input) ?: return null
        val projectId = match.groupValues[1]
        if (!configuration.projects.contains(projectId)) {
            return null
        }

        return "https://${configuration.subdomain}.atlassian.net/browse/${input}"
    }

    data class Configuration(val subdomain: String, val projects: Set<String>)

}
