package com.yihangho.jelly.handlers

import com.yihangho.jelly.services.SynonymsService
import com.yihangho.jelly.utils.tokenize
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(value = Ordered.HIGHEST_PRECEDENCE)
class GitHubHandler(
    private val configuration: Configuration,
    private val synonyms: SynonymsService
) : IHandler {

    override fun handle(input: String): String? {
        if (!Regex("""^gh(?:\s.*)?""").matches(input)) {
            return null
        }

        val tokens = tokenize(input, limit = 3)

        if (tokens.size == 1) {
            return "https://github.com"
        }

        val repo = synonyms.resolve(tokens[1], "repo") ?: tokens[1]
        return if (tokens.size == 2) {
            "https://github.com/${configuration.namespace}/${repo}"
        } else {
            "https://github.com/${configuration.namespace}/${repo}/commit/${tokens[2]}"
        }
    }

    data class Configuration(val namespace: String)

}
