package com.yihangho.jelly.handlers

interface IHandler {

    fun handle(input: String): String?

}
