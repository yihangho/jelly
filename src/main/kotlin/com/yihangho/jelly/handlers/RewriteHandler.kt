package com.yihangho.jelly.handlers

import com.yihangho.jelly.utils.isUrl
import com.yihangho.jelly.utils.parseAtAlias
import com.yihangho.jelly.utils.parseFirstTokenAndRest
import org.springframework.context.annotation.Lazy
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(value = 0)
class RewriteHandler(private val configuration: Configuration, @Lazy private val handlersService: HandlersService) :
    IHandler {

    override fun handle(input: String): String? {
        val (atAlias, rest) = parseFirstTokenAndRest(input) ?: return null
        val sourceUrl = if (isUrl(rest)) rest else handlersService.handle(rest)

        val alias = parseAtAlias(atAlias) ?: return null
        val match = findLongestMatch(sourceUrl) ?: return null

        val prefix = match.group[alias] ?: return null
        val suffix = sourceUrl.substring(match.prefix.length)

        return "$prefix$suffix"
    }

    private fun findLongestMatch(input: String): MatchCandidate? {
        return configuration
            .groups
            .values
            .asSequence()
            .map {
                val match = findLongestMatch(input, it) ?: return@map null
                MatchCandidate(match, it)
            }
            .filterNotNull()
            .maxByOrNull { it.prefix.length }
    }

    private fun findLongestMatch(input: String, group: Map<String, String>): String? {
        return group
            .values
            .asSequence()
            .filter { input.startsWith(it) }
            .maxByOrNull { it.length }
    }

    data class Configuration(val groups: Map<String, Map<String, String>>)

    private data class MatchCandidate(val prefix: String, val group: Map<String, String>)

}
