package com.yihangho.jelly.handlers

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(value = Ordered.LOWEST_PRECEDENCE)
class FallbackHandler : IHandler {

    override fun handle(input: String): String? {
        return "https://www.google.com/search?q=$input"
    }

}
