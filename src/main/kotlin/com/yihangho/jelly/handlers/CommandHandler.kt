package com.yihangho.jelly.handlers

import com.yihangho.jelly.utils.tokenize
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(value = Ordered.HIGHEST_PRECEDENCE)
class CommandHandler(private val configuration: Configuration) : IHandler {

    override fun handle(input: String): String? {
        val chunks = tokenize(input, limit = 2)
        val trigger = chunks.first()
        val args = chunks.getOrNull(1)

        val command = configuration.commands.find { it.trigger == trigger } ?: return null

        return if (command.urlTemplate != null && args != null) {
            command.urlTemplate.replace("%s", args)
        } else {
            command.url
        }
    }

    data class Configuration(val commands: List<Command>)

    data class Command(val trigger: String, val url: String, val urlTemplate: String?)

}
