package com.yihangho.jelly.handlers

import org.springframework.stereotype.Service

interface HandlersService {

    fun handle(input: String): String

}

@Service
class HandlersServiceImpl(private val handlers: List<IHandler>) : HandlersService {

    override fun handle(input: String): String {
        return handlers
            .asSequence()
            .map { it.handle(input) }
            .filterNotNull()
            .first()
    }

}
