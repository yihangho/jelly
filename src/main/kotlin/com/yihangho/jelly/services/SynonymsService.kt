package com.yihangho.jelly.services

import com.yihangho.jelly.properties.SynonymEntry
import com.yihangho.jelly.properties.SynonymsProperties
import org.springframework.stereotype.Component

interface SynonymsService {

    fun resolve(keyword: String, property: String): String?

}

@Component
class SynonymsServiceImpl(private val properties: SynonymsProperties) : SynonymsService {

    override fun resolve(keyword: String, property: String): String? {
        return findEntry(keyword)?.properties?.get(property)
    }

    private fun findEntry(keyword: String): SynonymEntry? {
        return properties.entries.firstOrNull { it.keywords.contains(keyword) }
    }

}
