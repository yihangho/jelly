package com.yihangho.jelly.controllers

import com.yihangho.jelly.handlers.HandlersService
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class SearchController(private val handlersService: HandlersService) {

    @GetMapping("/search")
    fun search(@RequestParam q: String): String {
        val target = handlersService.handle(q)

        return "redirect:$target"
    }

}
