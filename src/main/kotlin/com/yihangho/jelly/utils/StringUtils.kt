package com.yihangho.jelly.utils

fun parseFirstTokenAndRest(input: String): Pair<String, String>? {
    val wordAndRestRegex = Regex("""^(\S+)\s+(.*)""")

    val match = wordAndRestRegex.matchEntire(input) ?: return null
    return Pair(match.groupValues[1], match.groupValues[2])
}

fun parseAtAlias(input: String): String? {
    if (!input.startsWith('@')) {
        return null
    }

    return input.substring(1)
}

fun isUrl(input: String): Boolean {
    val urlRegex = Regex("""^https?://""")
    return urlRegex.containsMatchIn(input)
}

fun tokenize(input: String, limit: Int = Int.MAX_VALUE): List<String> {
    val output = mutableListOf<String>()
    val iterator = PeekableIterator(input.iterator())

    fun Char.isQuote(): Boolean {
        return this == '"' || this == '\''
    }

    fun consumeEscapableWord() {
        val terminal = iterator.next()
        val chars = mutableListOf<Char>()

        while (iterator.hasNext() && iterator.peek() != terminal) {
            if (iterator.peek() == '\\') {
                iterator.next() // Consume the escape char
            }

            if (iterator.hasNext()) {
                chars.add(iterator.next())
            }
        }

        if (iterator.hasNext()) { // Must be the terminal. Consume it.
            iterator.next()
        }

        output.add(chars.joinToString(""))
    }

    fun consumeWord() {
        val chars = mutableListOf<Char>()

        while (iterator.hasNext() && !iterator.peek().isWhitespace()) {
            chars.add(iterator.next())
        }

        output.add(chars.joinToString(""))
    }

    fun consumeRest() {
        val chars = mutableListOf<Char>()

        while (iterator.hasNext()) {
            chars.add(iterator.next())
        }

        output.add(chars.joinToString(""))
    }


    while (iterator.hasNext()) {
        if (iterator.peek().isWhitespace()) {
            iterator.next()
            continue
        }

        if (output.size + 1 == limit) {
            consumeRest()
        } else if (iterator.peek().isQuote()) {
            consumeEscapableWord()
        } else {
            consumeWord()
        }
    }

    return output
}
