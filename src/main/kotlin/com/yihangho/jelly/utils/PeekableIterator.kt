package com.yihangho.jelly.utils

class PeekableIterator<T>(private val iterator: Iterator<T>): Iterator<T> {
    private var peeked: T? = null
    override fun hasNext(): Boolean {
        return peeked != null || iterator.hasNext()
    }

    override fun next(): T {
        val output = peeked ?: iterator.next()
        peeked = null
        return output
    }

    fun peek(): T {
        if (peeked == null) {
            peeked = iterator.next()
        }

        return peeked!!
    }
}
