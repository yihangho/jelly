package com.yihangho.jelly.services

import com.yihangho.jelly.properties.SynonymEntry
import com.yihangho.jelly.properties.SynonymsProperties
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class SynonymsServiceTest {

    private val properties = SynonymsProperties(entries = listOf(
        SynonymEntry(
            keywords = setOf("John Doe", "JD"),
            properties = mapOf("id" to "jd", "firstName" to "John", "lastName" to "Doe")
        )
    ))

    private val service = SynonymsServiceImpl(properties)

    @Test
    fun `should resolve using any of the keywords`() {
        assertEquals("jd", service.resolve("John Doe", "id"))
        assertEquals("jd", service.resolve("JD", "id"))

        assertEquals("John", service.resolve("John Doe", "firstName"))
        assertEquals("John", service.resolve("JD", "firstName"))

        assertEquals("Doe", service.resolve("John Doe", "lastName"))
        assertEquals("Doe", service.resolve("JD", "lastName"))
    }

    @Test
    fun `should return null when the keyword is not known`() {
        assertNull(service.resolve("Jane Doe", "firstName"))
    }

    @Test
    fun `should return null if the property is not available`() {
        assertNull(service.resolve("John Doe", "email"))
    }

}
