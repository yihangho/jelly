package com.yihangho.jelly.utils

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class PeekableIteratorTest {

    @Test
    fun `PeekableIterator should work correctly`() {
        val iterator = PeekableIterator(listOf(1, 2, 3).iterator())

        assertTrue(iterator.hasNext())
        assertEquals(1, iterator.peek())
        assertEquals(1, iterator.peek())
        assertEquals(1, iterator.next())

        assertTrue(iterator.hasNext())
        assertEquals(2, iterator.peek())
        assertEquals(2, iterator.next())

        assertTrue(iterator.hasNext())
        assertEquals(3, iterator.peek())
        assertTrue(iterator.hasNext())
        assertEquals(3, iterator.next())

        assertFalse(iterator.hasNext())
    }

}
