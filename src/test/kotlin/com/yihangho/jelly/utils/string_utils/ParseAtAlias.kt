package com.yihangho.jelly.utils.string_utils

import com.yihangho.jelly.utils.parseAtAlias
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class ParseAtAlias {

    @Test
    fun `valid at alias`() {
        assertEquals("bla", parseAtAlias("@bla"))
    }

    @Test
    fun `not an at alias`() {
        assertNull(parseAtAlias("bla"))
    }

}
