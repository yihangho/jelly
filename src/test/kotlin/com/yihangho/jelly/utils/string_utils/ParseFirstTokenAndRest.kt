package com.yihangho.jelly.utils.string_utils

import com.yihangho.jelly.utils.parseFirstTokenAndRest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class ParseFirstTokenAndRest {

    @Test
    fun `valid first token and rest`() {
        val (f1, r1) = parseFirstTokenAndRest("hello    foo bar 123")!!
        assertEquals("hello", f1)
        assertEquals("foo bar 123", r1)

        val (f2, r2) = parseFirstTokenAndRest("@bla hello world")!!
        assertEquals("@bla", f2)
        assertEquals("hello world", r2)
    }

    @Test
    fun `not a first token and rest`() {
        assertNull(parseFirstTokenAndRest("foobar123"))
    }

}
