package com.yihangho.jelly.utils.string_utils

import com.yihangho.jelly.utils.tokenize
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Tokenize {

    @Test
    fun `simple cases`() {
        assertEquals(listOf<String>(), tokenize("     "))
        assertEquals(listOf("abc"), tokenize("   abc"))
        assertEquals(listOf("abc"), tokenize("abc"))
        assertEquals(listOf("abc", "def"), tokenize("abc   \t def"))
    }

    @Test
    fun `with limit`() {
        assertEquals(listOf("abc", "def \tghi"), tokenize("abc def \tghi", limit = 2))
        assertEquals(listOf("abc", "def", "ghi"), tokenize("abc def \tghi", limit = 10))
    }

    @Test
    fun `with escaping`() {
        assertEquals(listOf("abc", "def ghi"), tokenize("abc \"def ghi\""))
        assertEquals(listOf("abc", "def ghi"), tokenize("abc 'def ghi'"))
        assertEquals(listOf("abc", "def' ghi"), tokenize("abc 'def\\' ghi'"))
        assertEquals(listOf("abc", "def\" ghi"), tokenize("abc \"def\\\" ghi\""))
    }

}
