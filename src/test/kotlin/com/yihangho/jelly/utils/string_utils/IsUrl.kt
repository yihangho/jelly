package com.yihangho.jelly.utils.string_utils

import com.yihangho.jelly.utils.isUrl
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class IsUrl {

    @Test
    fun `detect if input is a url`() {
        assertTrue(isUrl("http://example.com"))
        assertTrue(isUrl("https://example.com"))

        assertFalse(isUrl("http:bla"))
        assertFalse(isUrl("ahttp://example.com"))
        assertFalse(isUrl("bla"))
    }


}
