package com.yihangho.jelly.handlers

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HandlersServiceTest {

    private val nullHandler = object : IHandler {
        override fun handle(input: String) = null
    }

    private val commandHandler = object : IHandler {
        override fun handle(input: String): String? {
            if (input.startsWith("s")) {
                return "http://example.com/cmd/$input"
            }

            return null
        }
    }

    private val fallbackHandler = object : IHandler {
        override fun handle(input: String) = "http://example.com/fallback/$input"
    }

    @Test
    fun `use the result of the first handler returning non-null`() {
        val handlersService = HandlersServiceImpl(listOf(nullHandler, commandHandler, fallbackHandler))

        assertEquals("http://example.com/cmd/s 123", handlersService.handle("s 123"))
        assertEquals("http://example.com/fallback/bla", handlersService.handle("bla"))
    }

}
