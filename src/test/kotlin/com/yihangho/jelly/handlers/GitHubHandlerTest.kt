package com.yihangho.jelly.handlers

import com.yihangho.jelly.services.SynonymsService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class GitHubHandlerTest {

    private val handler = GitHubHandler(
        GitHubHandler.Configuration(namespace = "example"),
        object : SynonymsService {
            override fun resolve(keyword: String, property: String): String? {
                if (keyword == "fs" && property == "repo") {
                    return "foo-service"
                }
                return null
            }
        }
    )

    @Test
    fun `no a gh prefix`() {
        assertNull(handler.handle("bla"))
        assertNull(handler.handle("gha"))
    }

    @Test
    fun `1 token`() {
        assertEquals("https://github.com", handler.handle("gh"))
    }

    @Test
    fun `2 tokens`() {
        assertEquals("https://github.com/example/bla", handler.handle("gh bla"))
        assertEquals("https://github.com/example/foo-service", handler.handle("gh fs"))
    }

    @Test
    fun `3 tokens`() {
        assertEquals("https://github.com/example/bla/commit/123", handler.handle("gh bla 123"))
        assertEquals("https://github.com/example/foo-service/commit/123", handler.handle("gh fs 123"))
    }

}
