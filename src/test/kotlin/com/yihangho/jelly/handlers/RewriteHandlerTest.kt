package com.yihangho.jelly.handlers

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class RewriteHandlerTest {

    private val config = RewriteHandler.Configuration(
        mapOf(
            "public" to mapOf(
                "dev" to "http://localhost",
                "prod" to "http://example.com"
            ),
            "admin" to mapOf(
                "dev" to "http://localhost/admin",
                "prod" to "http://admin.example.com"
            )
        )
    )

    private val handlersService = object : HandlersService {

        override fun handle(input: String): String {
            return if (input == "s") {
                "http://localhost/bla"
            } else {
                input
            }
        }

    }

    private val handler = RewriteHandler(config, handlersService)

    @Test
    fun `not starting with an at-alias`() {
        assertNull(handler.handle("bla"))
        assertNull(handler.handle("dev http://example.com"))
    }

    @Test
    fun `not a known at-alias`() {
        assertNull(handler.handle("@d http://example.com"))
    }

    @Test
    fun `no match`() {
        assertNull(handler.handle("@dev http://public.example.com"))
    }

    @Test
    fun `keeps the suffix when there is a match`() {
        assertEquals("http://example.com/page/123", handler.handle("@prod http://localhost/page/123"))
        assertEquals("http://localhost/admin/user/456", handler.handle("@dev http://admin.example.com/user/456"))
    }

    @Test
    fun `uses the longest match`() {
        // http://localhost/admin matches both `public` and `admin`, but `admin` is the longer match
        assertEquals("http://admin.example.com", handler.handle("@prod http://localhost/admin"))
        assertEquals("http://admin.example.com/user/456", handler.handle("@prod http://localhost/admin/user/456"))
    }

    @Test
    fun `use handlers service to resolve into a url when possible`() {
        assertEquals("http://example.com/bla", handler.handle("@prod s"))
        assertNull(handler.handle("@prod b"))
    }

}
