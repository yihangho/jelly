package com.yihangho.jelly.handlers

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class CommandHandlerTest {

    private val config = CommandHandler.Configuration(
        listOf(
            CommandHandler.Command(
                trigger = "s",
                url = "http://example.com",
                urlTemplate = "http://example.com/search?q=%s"
            ),
            CommandHandler.Command(trigger = "g", url = "http://example.org", urlTemplate = null)
        )
    )

    private val handler = CommandHandler(config)

    @Test
    fun `no match`() {
        assertNull(handler.handle("h 123"))
    }

    @Test
    fun `match with and without argument`() {
        assertEquals("http://example.com", handler.handle("s"))
        assertEquals("http://example.com/search?q=hello world", handler.handle("s hello world"))
    }

    @Test
    fun `match command without template`() {
        assertEquals("http://example.org", handler.handle("g"))
        assertEquals("http://example.org", handler.handle("g hello world"))
    }

}
