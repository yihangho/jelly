FROM azul/zulu-openjdk:17 AS builder

WORKDIR /jelly
COPY . .

RUN ./gradlew bootJar

FROM azul/zulu-openjdk-alpine:17-jre

ENV spring_profiles_active=production
EXPOSE 8080

WORKDIR /jelly
COPY --from=builder /jelly/build/libs/jelly-0.0.1-SNAPSHOT.jar jelly.jar

CMD ["java", "-jar", "jelly.jar"]
